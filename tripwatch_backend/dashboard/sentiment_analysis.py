from nltk.stem.wordnet import WordNetLemmatizer
from nltk.corpus import twitter_samples, stopwords
from nltk.tag import pos_tag
from nltk.tokenize import word_tokenize
from nltk import FreqDist, classify, NaiveBayesClassifier

import dashboard.twilio_connection as twilio_connection

import dashboard.views as views
import requests 
import uuid 
import re
from .models import FeedbackGiven
import time
from datetime import datetime,timedelta
import pandas

import re, string, random


url = "http://localhost:8000/dash/api/feedback/"

def remove_noise(tweet_tokens, stop_words = ()):

    cleaned_tokens = []

    # pos_tag method tags the token as a noun,verb etc
    for token, tag in pos_tag(tweet_tokens):  
        if tag.startswith("NN"):
            pos = 'n'
        elif tag.startswith('VB'):
            pos = 'v'
        else:
            pos = 'a'

        #looks roots of words, nouns and etc and changes it to its root form
        lemmatizer = WordNetLemmatizer()
        token = lemmatizer.lemmatize(token, pos)
    
        #remove the number plate from the list of tokens
        token = re.sub("[Kk][a-zA-Z]{2}[0-9]{3}[a-zA-Z]{1}[a-zA-Z]*","", token)

        #if token is not empty, and its not a punctuation mark, and its not a stop word, its added as a clean word
        if len(token) > 0 and token not in string.punctuation and token.lower() not in stop_words:
            cleaned_tokens.append(token.lower())
    return cleaned_tokens

#method to combine all tweet objects into one object
def get_all_words(cleaned_tokens_list):
    for tokens in cleaned_tokens_list:
        for token in tokens:
            yield tokens

#convert the feedback from a list of cleaned tokens to dictionaries 
# with keys as the tokens and True as values. 
def get_tweets_for_model(cleaned_tokens_list):
    for tweet_tokens in cleaned_tokens_list:
        yield dict([token, True] for token in tweet_tokens)

# def admin_choose_sentiment(feedback):
#     return feedback
#     print("admin will choose what the feedback implies :)")

def check_if_string_in_file(file_name, string_to_search):
    """ Check if any line in the file contains given string """
    # Open the file in read only mode
    with open(file_name, 'r') as read_obj:
        # Read all lines in the file one by one
        for line in read_obj:
            # For each line, check if line contains the string
            if string_to_search in line:
                return True
    return False

def training(tokens):
    pos_count = 0
    with open('E:\\Computer Science Stuff\\Project\\TripWatch\\tripwatch_backend\\dashboard\\positive_feedback.txt', 'r') as myfile:
        for line in myfile:
            pos_count+=1
    print (pos_count)

    neg_count = 0
    with open('E:\\Computer Science Stuff\\Project\\TripWatch\\tripwatch_backend\\dashboard\\negative_feedback.txt', 'r') as myfile:
        for line in myfile:
            neg_count+=1
    print (neg_count)

    #file with positive feedback for training
    with open('E:\\Computer Science Stuff\\Project\\TripWatch\\tripwatch_backend\\dashboard\\positive_feedback.txt', 'r') as myfile:
        positive_strings = [positive_strings.strip() for positive_strings in myfile]

    #file with positive feedback for training
    with open('E:\\Computer Science Stuff\\Project\\TripWatch\\tripwatch_backend\\dashboard\\negative_feedback.txt', 'r') as myfile:
        negative_strings = [negative_strings.strip() for negative_strings in myfile]

    #tokenization, A token is a sequence of characters in text that
    #  serves as a unit. Based on how you create the tokens, they may 
    # consist of words, emoticons, hashtags, links, or even individual characters.

    #remove the most common words in a language are called stop words. 
    # Some examples of stop words are “is”, “the”, and “a”. They are generally 
    # irrelevant when processing language, unless a specific use case warrants 
    # their inclusion.
    # with open('swahili.txt', 'r') as myfile:
    #     positive_strings = myfile.read()
    stop_words = stopwords.words('swahili.txt')

    #splits the whole sentence into different tokens or elements, incuding characters
    positive_feed_token = [word_tokenize(line) for line in positive_strings]
    negative_feed_token = [word_tokenize(line) for line in negative_strings]

    positive_cleaned_tokens_list = []
    negative_cleaned_tokens_list = []

    #loop through the tokens objects, and remove the characters, then add the object to the cleaned list
    for tokens in positive_feed_token:
        positive_cleaned_tokens_list.append(remove_noise(tokens, stop_words))
    
    for tokens in negative_feed_token:
        negative_cleaned_tokens_list.append(remove_noise(tokens, stop_words))

    #return all the tokenized objects*
    all_pos_words = get_all_words(positive_cleaned_tokens_list)
    all_neg_words = get_all_words(negative_cleaned_tokens_list)

    #you can find out which are the most common words using the FreqDist class of NLTK.
    #The .most_common() method lists the words which occur most frequently in the data and number
    #of times they appear
    # freq_dist_pos = FreqDist(all_pos_words)
    # print(freq_dist_pos.most_common(10))

    #convert the lists to dictionaries with the token as the key and true as the value
    positive_tokens_for_model = get_tweets_for_model(positive_cleaned_tokens_list)
    negative_tokens_for_model = get_tweets_for_model(negative_cleaned_tokens_list)

    #Attaches a Positive or Negative label to each tweet.,adds a new "row" to the dict

    positive_dataset = [(tweet_dict, "Positive")
                         for tweet_dict in positive_tokens_for_model]

    negative_dataset = [(tweet_dict, "Negative")
                         for tweet_dict in negative_tokens_for_model]
    
    #creates a dataset by joining the positive and negative tweets.
    dataset = positive_dataset + negative_dataset
    
    #By default, the data contains all positive tweets followed by all negative tweets 
    # in sequence. When training the model, you should provide a sample of your data that 
    # does not contain any bias. To avoid bias, you’ve added code to randomly arrange the 
    # data using the .shuffle() method of random.
    random.shuffle(dataset)

    #he code splits the shuffled data into a ratio of 70:30 for training and testing, respectively.
    train_data = dataset[:1800]
    test_data = dataset[400:]

    #use the NaiveBayesClassifier class to build the model. 
    # Use the .train() method to train the model and the
    classifier = NaiveBayesClassifier.train(train_data)

    #  .accuracy() method tests the model on the testing data.
    #Accuracy is defined as the percentage of tweets in the testing
    # dataset for which the model was correctly able to predict the sentiment
    print("Accuracy is:", classify.accuracy(classifier, test_data))

    #every row in the output shows the ratio of occurrence of a token 
    # in positive and negative tagged tweets in the training dataset
    #shows words associated with a particular sentiment
    print(classifier.show_most_informative_features())
    sentiment = classifier.classify(dict([token, True] for token in tokens))
    return sentiment

    #method to fetch messages, call classifier

def message_classification():
    for myrecord in twilio_connection.messages:
        # print(datetime.today().date())
        today = datetime.now()
        # last_hour = datetime.now() - timedelta(hours = 3)
        
        if myrecord.direction == "inbound" and myrecord.date_sent.date() == today.date():
            feedback_received = myrecord.body

            daysent = myrecord.date_sent

            # Get the date object from datetime object
            dateObj = daysent.date()
            timeObj = daysent.time()
            feedback = myrecord.body
            
            if FeedbackGiven.objects.filter(day_received=dateObj, time_sent=timeObj).exists():
                break
            else:
                #do analysis on feedback body to see if it meet format
                #1.Check if number plate is present
                expected_format = '[Kk][a-zA-Z]{2}[0-9]{3}[a-zA-Z]{1}[a-zA-Z]*'
                pattern = re.compile(expected_format)

                if(pattern.match(feedback)):
                    custom_tokens = remove_noise(word_tokenize(feedback_received))
                    # print(custom_tokens, classfier.classify(dict([token, True] for token in custom_tokens)))
                    
                    check_if_word_exists_in_file(myrecord)
                    #classify token
                    sentiment = training(custom_tokens)
                    print(sentiment)
                    
                    #create a pass the string that isnt known,wwhere the admin puts the sentment,andits stored in the files.

                    # 3. Get number plate from tex
                    number_plate = feedback.split()
                    number_plate = [ i.upper() for i in number_plate]
                    #4. Post the feedback  
                    
                    myobj = {'day_received': dateObj,
                                'time_sent': timeObj,
                                'number_plate': number_plate[0],
                                'feedback_body': feedback,
                                'sentiment': sentiment}
                    
                    # if FeedbackGiven.objects.filter(day_received=dateObj, time_sent=timeObj).exists():
                    #     print("not posted")
                    # else:
                    x = requests.post(url, data = myobj)
                    print('success!')
                    store_new_feed(feedback_received,sentiment)
                    #     if sentiment == "Positive":
                    #         with open("E:\\Computer Science Stuff\\Project\\TripWatch\\tripwatch_backend\\dashboard\\positive_feedback.txt", "a") as text_file:
                    #             text_file.write("\n"+feedback_received)
                    #     elif sentiment == "Negative":
                    #         with open("E:\\Computer Science Stuff\\Project\\TripWatch\\tripwatch_backend\\dashboard\\negative_feedback.txt", "a") as text_file:
                    #             text_file.write("\n"+feedback_received)

def check_if_word_exists_in_file(record):
    #check if string exists in the files,if yes,continue                    
    posfile = "E:\\Computer Science Stuff\\Project\\TripWatch\\tripwatch_backend\\dashboard\\positive_feedback.txt"
    negfile = "E:\\Computer Science Stuff\\Project\\TripWatch\\tripwatch_backend\\dashboard\\negative_feedback.txt"
    
    feedback_received = record.body

    words = feedback_received.split()
    words.pop(0)
    for i in words:
        print(i)
        if check_if_string_in_file(posfile,i) or check_if_string_in_file(negfile,i):
            return True
            continue
        else:
            return False
            # views.get_record(record)
    

def store_new_feed(message,sentiment):
    if sentiment == "Positive":
        with open("E:\\Computer Science Stuff\\Project\\TripWatch\\tripwatch_backend\\dashboard\\positive_feedback.txt", "a") as text_file:
            text_file.write("\n"+message)
    elif sentiment == "Negative":
        with open("E:\\Computer Science Stuff\\Project\\TripWatch\\tripwatch_backend\\dashboard\\negative_feedback.txt", "a") as text_file:
            text_file.write("\n"+message)
    

# def admin_analysis(record_to_be_classified):

#     pass

