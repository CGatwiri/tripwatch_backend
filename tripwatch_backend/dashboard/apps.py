from django.apps import AppConfig


class DashboardConfig(AppConfig):
    name = 'dashboard'

    def ready(self):
        pass
        # write your startup code here you can import application code here
        # from dashboard.feedback_processing import fetch_and_analyse_new_messages