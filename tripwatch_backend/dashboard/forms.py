from django import forms
from .models import Drivers, Conductors, Vehicles, FeedbackGiven, Saccos

class DriversForm(forms.ModelForm):
    class Meta:
        model = Drivers
        fields = ('driver_name','number_plate','year_joined','driver_picture')

class ConductorsForm(forms.ModelForm):
    class Meta:
        model = Conductors
        fields = ('conductor_name','number_plate','sacco_name','year_joined','conductor_picture')

class VehicleForm(forms.ModelForm):
    class Meta:
        model = Vehicles
        fields = ('number_plate','sacco_name')

class FeedbackGivenForm(forms.ModelForm):
    class Meta:
        model = FeedbackGiven
        fields = ('day_received','time_sent','number_plate','feedback_body','sentiment',)

# class SearchSaccoForm(forms.ModelForm):
#     class Meta:
#         model = Saccos
#         fields = ('sacco_name',