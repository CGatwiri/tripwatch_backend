from __future__ import unicode_literals

from django.http import HttpResponse
from rest_framework import generics, views, viewsets
from rest_framework.renderers import TemplateHTMLRenderer
from rest_framework.response import Response
from rest_framework.views import APIView
from django import template
from datetime import datetime    
from django.shortcuts import redirect
from .serializers import FeedbackGivenSerializer, SaccosSerializer,MatatuRoutesSerializer, VehiclesSerializer, DriversSerializer, ConductorsSerializer
from .models import FeedbackGiven, Saccos, MatatuRoutes, Vehicles, Drivers, Conductors
from .forms import FeedbackGivenForm
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.csrf import csrf_protect
from .forms import DriversForm,ConductorsForm,VehicleForm
from django.http import HttpResponseRedirect
from django.conf import settings
from django.db.models.functions import Cast
from django.db.models import CharField
from twilio.twiml.messaging_response import MessagingResponse
import dashboard.twilio_connection as twilio_connection
import re


from django.shortcuts import render,HttpResponse

import dashboard.sentiment_analysis as sentiment_analysis
# import dashboard.feedback_processing as feed_analysis

def get_record(record):
    return record

def admin_feedback_today(request):
    for myrecord in twilio_connection.messages:
        feedback = myrecord.body
        expected_format = '[Kk][a-zA-Z]{2}[0-9]{3}[a-zA-Z]{1}[a-zA-Z]*'
        pattern = re.compile(expected_format)

        if(pattern.match(feedback)):
            if not sentiment_analysis.check_if_word_exists_in_file(myrecord):
                unclassified_list = []
                # feed_today = sentiment_analysis.message_classification()
                unclassified_list.append(myrecord)
                print(unclassified_list)
                # if this is a POST request we need to process the form data
                if request.method == 'POST':
                    # create a form instance and populate it with data from the request:
                    form = FeedbackGivenForm(request.POST)
                    # check whether it's valid:
                    if form.is_valid():
                        myform = form.save(commit=False)
                        myform.save()
                        print('success')
                        # return redirect('adminhome')
                        # process the data in form.cleaned_data as required
                        # ...
                        # redirect to a new URL:
                        return HttpResponseRedirect('adminhome')

            #     # if a GET (or any other method) we'll create a blank form
                else:
                    form = FeedbackGivenForm()

                return render(request, 'dashboard/unclassified.html', {'form': form,'feed_today': unclassified_list})
        else:
            return render(request,'dashboard/unclassified.html')


def adminhome(request):
    return render(request,'dashboard/myhome.html')

def index(request,**kwargs):
    
    sentiment_analysis.message_classification()
    time_period = '4 HOURS'
    driverfeedtoday = Drivers.objects.raw(
        '''SELECT * FROM dashboard_drivers 
           INNER JOIN dashboard_feedbackgiven ON dashboard_drivers.number_plate = dashboard_feedbackgiven.number_plate
           INNER JOIN dashboard_vehicles ON dashboard_feedbackgiven.number_plate = dashboard_vehicles.number_plate
           INNER JOIN dashboard_saccos ON dashboard_vehicles.sacco_name = dashboard_saccos.sacco_name
           WHERE cast(dashboard_feedbackgiven.day_received as date) = current_date ''')
           
    pos = "Positive"
    neg = "Negative"
    drivers_ranking = Drivers.objects.raw(
        '''SELECT dashboard_drivers.id,dashboard_drivers.driver_name,dashboard_vehicles.sacco_name,
                count(*) AS total,
                sum(case when dashboard_feedbackgiven.sentiment = %s then 1 else 0 end) AS PosCount,
                sum(case when dashboard_feedbackgiven.sentiment = %s then 1 else 0 end) AS NegCount
            FROM public.dashboard_feedbackgiven
            INNER JOIN public.dashboard_drivers
                ON dashboard_feedbackgiven.number_plate = dashboard_drivers.number_plate
            INNER JOIN public.dashboard_vehicles
                ON dashboard_feedbackgiven.number_plate = dashboard_vehicles.number_plate
            WHERE cast(dashboard_feedbackgiven.day_received as date) = current_date
            GROUP BY dashboard_drivers.id,dashboard_drivers.driver_name,dashboard_vehicles.sacco_name
        ''',[pos,neg]
    )

    default_rank = 5
    for d in drivers_ranking:
        pos_rate = d.poscount
        neg_rate = d.negcount
        new_count = default_rank+(pos_rate*0.25)-(neg_rate*0.25)

        d.count = new_count

    pos = "Positive"
    neg = "Negative"
    saccos_ranking = Saccos.objects.raw(
        '''SELECT dashboard_saccos.id,dashboard_saccos.sacco_name,dashboard_saccos.route_number,
            count(*) AS total,
                sum(case when dashboard_feedbackgiven.sentiment = %s then 1 else 0 end) AS PosCount,
                sum(case when dashboard_feedbackgiven.sentiment = %s then 1 else 0 end) AS NegCount
            FROM public.dashboard_saccos
            INNER JOIN public.dashboard_vehicles
                ON dashboard_vehicles.sacco_name = dashboard_saccos.sacco_name
            INNER JOIN public.dashboard_feedbackgiven
                ON dashboard_feedbackgiven.number_plate = dashboard_vehicles.number_plate
            WHERE cast(dashboard_feedbackgiven.day_received as date) = current_date
            GROUP BY dashboard_saccos.id,dashboard_saccos.sacco_name,dashboard_saccos.route_number
        ''',[pos,neg]
    )

    default_rank = 5
    for d in saccos_ranking:
        pos_rate = d.poscount
        neg_rate = d.negcount
        new_count = default_rank+(pos_rate*0.25)-(neg_rate*0.25)

        d.count = new_count
    
    # todayfeed = FeedbackGiven.objects.filter(day_received=datetime.date(datetime.now()),feedback_about = "driver")

    return render(request, 'dashboard/index.html', {'criticalfeed' :driverfeedtoday,
                                                    'drivers_ranking' : drivers_ranking,
                                                    'saccos_ranking': saccos_ranking,
                                                    'media_url':settings.MEDIA_URL})
def weekly_report(request,**kwargs):
    about = "driver"
    condabout = "conductor"
    vehicleabout = "vehicle"

    pos = "Positive"
    neg = "Negative"
    drivers_ranking = Drivers.objects.raw(
        '''SELECT dashboard_drivers.id,dashboard_drivers.driver_name,dashboard_vehicles.sacco_name,
                count(*) AS total,
                sum(case when dashboard_feedbackgiven.sentiment = %s then 1 else 0 end) AS PosCount,
                sum(case when dashboard_feedbackgiven.sentiment = %s then 1 else 0 end) AS NegCount
            FROM public.dashboard_feedbackgiven
            INNER JOIN public.dashboard_drivers
                ON dashboard_feedbackgiven.number_plate = dashboard_drivers.number_plate
            INNER JOIN public.dashboard_vehicles
                ON dashboard_feedbackgiven.number_plate = dashboard_vehicles.number_plate
            WHERE cast(dashboard_feedbackgiven.day_received as date) >= now() - interval '1 week'
            GROUP BY dashboard_drivers.id,dashboard_drivers.driver_name,dashboard_vehicles.sacco_name
        ''',[pos,neg]
    )

    default_rank = 5
    for d in drivers_ranking:
        pos_rate = d.poscount
        neg_rate = d.negcount
        new_count = default_rank+(pos_rate*0.25)-(neg_rate*0.25)

        d.count = new_count
    

    pos = "Positive"
    neg = "Negative"
    saccos_ranking = Saccos.objects.raw(
        '''SELECT dashboard_saccos.id,dashboard_saccos.sacco_name,dashboard_saccos.route_number,
            count(*) AS total,
                sum(case when dashboard_feedbackgiven.sentiment = %s then 1 else 0 end) AS PosCount,
                sum(case when dashboard_feedbackgiven.sentiment = %s then 1 else 0 end) AS NegCount
            FROM public.dashboard_saccos
            INNER JOIN public.dashboard_vehicles
                ON dashboard_vehicles.sacco_name = dashboard_saccos.sacco_name
            INNER JOIN public.dashboard_feedbackgiven
                ON dashboard_feedbackgiven.number_plate = dashboard_vehicles.number_plate
            WHERE cast(dashboard_feedbackgiven.day_received as date) >= now() - interval '1 week'
            GROUP BY dashboard_saccos.id,dashboard_saccos.sacco_name,dashboard_saccos.route_number
        ''',[pos,neg]
    )

    default_rank = 5
    for d in saccos_ranking:
        pos_rate = d.poscount
        neg_rate = d.negcount
        new_count = default_rank+(pos_rate*0.25)-(neg_rate*0.25)

        d.count = new_count

    vehiclefeedtoday = Conductors.objects.raw(
        '''SELECT * FROM dashboard_vehicles 
           INNER JOIN dashboard_feedbackgiven ON dashboard_vehicles.number_plate = dashboard_feedbackgiven.number_plate 
           WHERE dashboard_feedbackgiven.feedback_about = %s''',[vehicleabout])
   
    return render(request, 'dashboard/weekly_report.html', {'feedback' :drivers_ranking,
                                                    'sacco': saccos_ranking,
                                                    'media_url':settings.MEDIA_URL})
def monthly_report(request,**kwargs):
   
    pos = "Positive"
    neg = "Negative"
    driverfeedtoday = Drivers.objects.raw(
        '''SELECT dashboard_drivers.id,dashboard_drivers.driver_name,dashboard_vehicles.sacco_name,
                count(*) AS total,
                sum(case when dashboard_feedbackgiven.sentiment = %s then 1 else 0 end) AS PosCount,
                sum(case when dashboard_feedbackgiven.sentiment = %s then 1 else 0 end) AS NegCount
            FROM public.dashboard_feedbackgiven
            INNER JOIN public.dashboard_drivers
                ON dashboard_feedbackgiven.number_plate = dashboard_drivers.number_plate
            INNER JOIN public.dashboard_vehicles
                ON dashboard_feedbackgiven.number_plate = dashboard_vehicles.number_plate
            WHERE cast(dashboard_feedbackgiven.day_received as date) >= date_trunc('month', current_date)
            GROUP BY dashboard_drivers.id,dashboard_drivers.driver_name,dashboard_vehicles.sacco_name
            ''',[pos,neg])
    
    default_rank = 5
    for d in driverfeedtoday:
        pos_rate = d.poscount
        neg_rate = d.negcount
        new_count = default_rank+(pos_rate*0.25)-(neg_rate*0.25)

        d.count = new_count
    
    saccos_ranking = Saccos.objects.raw(
        '''SELECT dashboard_saccos.id,dashboard_saccos.sacco_name,dashboard_saccos.route_number,
            count(*) AS total,
                sum(case when dashboard_feedbackgiven.sentiment = %s then 1 else 0 end) AS PosCount,
                sum(case when dashboard_feedbackgiven.sentiment = %s then 1 else 0 end) AS NegCount
            FROM public.dashboard_saccos
            INNER JOIN public.dashboard_vehicles
                ON dashboard_vehicles.sacco_name = dashboard_saccos.sacco_name
            INNER JOIN public.dashboard_feedbackgiven
                ON dashboard_feedbackgiven.number_plate = dashboard_vehicles.number_plate
            WHERE cast(dashboard_feedbackgiven.day_received as date) >= date_trunc('month', current_date)
            GROUP BY dashboard_saccos.id,dashboard_saccos.sacco_name,dashboard_saccos.route_number
        ''',[pos,neg]
    )

    default_rank = 5
    for d in saccos_ranking:
        pos_rate = d.poscount
        neg_rate = d.negcount
        new_count = default_rank+(pos_rate*0.25)-(neg_rate*0.25)

        d.count = new_count
   
   
    return render(request, 'dashboard/monthly_report.html', {'feedback' :driverfeedtoday,
                                                    'myobj': saccos_ranking,
                                                    'media_url':settings.MEDIA_URL})

def vehicle(request):
    about = "vehicle"
    vehiclerepeat = Vehicles.objects.raw(
        '''SELECT * FROM dashboard_vehicles 
        INNER JOIN dashboard_feedbackgiven ON dashboard_vehicles.number_plate = dashboard_feedbackgiven.number_plate 
        WHERE dashboard_feedbackgiven.feedback_about = %s''',[about])
    
   
    return render(request, 'dashboard/vehicle.html', {'veh_obj': vehiclerepeat,
                                                      'media_url':settings.MEDIA_URL})

def home(request):
    # feed_analysis.fetch_and_analyse_new_messages()
    
    return render(request,'dashboard/home.html')

def drivers(request):
    about = "driver"

    pos = "Positive"
    neg = "Negative"
    drivers_ranking = Drivers.objects.raw(
        '''SELECT dashboard_drivers.id,dashboard_drivers.driver_name,dashboard_vehicles.sacco_name,
            count(*) AS total,
            sum(case when dashboard_feedbackgiven.sentiment = %s then 1 else 0 end) AS PosCount,
            sum(case when dashboard_feedbackgiven.sentiment = %s then 1 else 0 end) AS NegCount
            FROM public.dashboard_feedbackgiven
            INNER JOIN public.dashboard_drivers
            ON dashboard_feedbackgiven.number_plate = dashboard_drivers.number_plate
            INNER JOIN public.dashboard_vehicles
            ON dashboard_feedbackgiven.number_plate = dashboard_vehicles.number_plate
            GROUP BY dashboard_drivers.id,dashboard_drivers.driver_name,dashboard_vehicles.sacco_name
        ''',[pos,neg]
    )

    default_rank = 5
    for d in drivers_ranking:
        pos_rate = d.poscount
        neg_rate = d.negcount
        new_count = default_rank+(pos_rate*0.25)-(neg_rate*0.25)

        d.count = new_count

    return render(request,'dashboard/driver_reports.html', {'obj': drivers_ranking,
                                                            'media_url':settings.MEDIA_URL})


def conductor(request):
    #Conductor rankings
    pos = "Positive"
    neg = "Negative"
    conductors_rankings = Conductors.objects.raw(
        '''SELECT dashboard_conductors.conductor_name,dashboard_conductors.id,dashboard_conductors.number_plate,
            dashboard_conductors.conductor_picture,dashboard_vehicles.sacco_name,
            count(*) AS total,
            sum(case when dashboard_feedbackgiven.sentiment = %s then 1 else 0 end) AS PosCount,
            sum(case when dashboard_feedbackgiven.sentiment = %s then 1 else 0 end) AS NegCount
            FROM public.dashboard_feedbackgiven
            INNER JOIN public.dashboard_conductors 
            ON public.dashboard_feedbackgiven.number_plate = public.dashboard_conductors.number_plate
            INNER JOIN public.dashboard_vehicles
            ON dashboard_feedbackgiven.number_plate = dashboard_vehicles.number_plate
            GROUP BY dashboard_conductors.conductor_name,dashboard_conductors.number_plate,
            dashboard_conductors.conductor_picture,dashboard_conductors.id,dashboard_vehicles.sacco_name
        ''',[pos,neg])

    default_rank = 5
    for d in conductors_rankings:
        pos_rate = d.poscount
        neg_rate = d.negcount
        new_count = default_rank+(pos_rate*0.25)-(neg_rate*0.25)

        d.count = new_count

    return render(request,'dashboard/conductor_reports.html', {'condobj':conductors_rankings,'media_url':settings.MEDIA_URL})

def specific_staff(request):
    return render(request, 'dashboard/specific_staff_report.html')

def stageman(request):
    return render(request, 'dashboard/stage_manager_reports.html',{'stageobj': StageManager.objects.all(),'media_url':settings.MEDIA_URL})
    
def sacco(request):

    pos = "Positive"
    neg = "Negative"
    saccos_ranking = Saccos.objects.raw(
        '''SELECT dashboard_saccos.id,dashboard_saccos.sacco_name,dashboard_saccos.route_number,
            count(*) AS total,
                sum(case when dashboard_feedbackgiven.sentiment = %s then 1 else 0 end) AS PosCount,
                sum(case when dashboard_feedbackgiven.sentiment = %s then 1 else 0 end) AS NegCount
            FROM public.dashboard_saccos
            INNER JOIN public.dashboard_vehicles
                ON dashboard_vehicles.sacco_name = dashboard_saccos.sacco_name
            INNER JOIN public.dashboard_feedbackgiven
                ON dashboard_feedbackgiven.number_plate = dashboard_vehicles.number_plate
            GROUP BY dashboard_saccos.id,dashboard_saccos.sacco_name,dashboard_saccos.route_number
        ''',[pos,neg]
    )

    default_rank = 5
    for d in saccos_ranking:
        pos_rate = d.poscount
        neg_rate = d.negcount
        new_count = default_rank+(pos_rate*0.25)-(neg_rate*0.25)

        d.count = new_count

    return render(request, 'dashboard/sacco_reports.html', {'saccoobj': saccos_ranking,
                                                            'media_url':settings.MEDIA_URL})

def repeat_sacco(request):
    sentiment_vehicle = "Negative"

    neg_sacco_vehicles = Vehicles.objects.raw(
        '''SELECT dashboard_vehicles.sacco_name,dashboard_saccos.route_number,dashboard_vehicles.id,dashboard_vehicles.number_plate,COUNT(*)
            FROM dashboard_vehicles
            INNER JOIN dashboard_saccos
            ON public.dashboard_vehicles.sacco_name = public.dashboard_saccos.sacco_name
            INNER JOIN dashboard_feedbackgiven
            ON public.dashboard_feedbackgiven.number_plate = public.dashboard_vehicles.number_plate
            WHERE dashboard_feedbackgiven.sentiment = %s
            GROUP BY dashboard_vehicles.number_plate,dashboard_vehicles.id,dashboard_saccos.route_number,dashboard_vehicles.sacco_name
        ''',[sentiment_vehicle])

    for v in neg_sacco_vehicles:
        orig_count = v.count 
        new_count = 5 -(orig_count*0.5)
        v.count = new_count

    n_sacco_feed = "Negative"    
    neg_saccos = Saccos.objects.raw(
            '''SELECT dashboard_saccos.sacco_name,dashboard_saccos.id,COUNT(*)
                FROM dashboard_saccos
                INNER JOIN dashboard_vehicles
                ON public.dashboard_vehicles.sacco_name = public.dashboard_saccos.sacco_name
                INNER JOIN dashboard_feedbackgiven
                ON public.dashboard_feedbackgiven.number_plate = public.dashboard_vehicles.number_plate
                WHERE dashboard_feedbackgiven.sentiment = %s
                GROUP BY dashboard_saccos.id,dashboard_saccos.sacco_name
            ''',[n_sacco_feed])
    
    for a in neg_saccos:
            orig_count = a.count 
            new_count = 5 -(orig_count*0.5)
            a.count = new_count

    return render(request, 'dashboard/repeat_offend_sacco.html', {'veh_obj': neg_sacco_vehicles,
                                                                'neg_saccos' : neg_saccos,
                                                                'media_url':settings.MEDIA_URL})

def harrassment(request):
    # harrassment_feedback = FeedbackGiven.objects.raw(
    #     '''SELECT * FROM dashboard_feedbackgiven 
    #        INNER JOIN dashboard_vehicles ON dashboard_feedbackgiven.number_plate = dashboard_vehicles.number_plate
    #        INNER JOIN dashboard_saccos ON LOWER(dashboard_vehicles.sacco_name) = LOWER(dashboard_saccos.sacco_name)''')

    harrassment_feedback = FeedbackGiven.objects.raw(
        '''SELECT * FROM dashboard_feedbackgiven
            WHERE (CONTAINS(dashboard_feedbackgiven.feedback_body, 'disabled')
            OR CONTAINS(dashboard_feedbackgiven.feedback_body, 'blind'))
            ''')
    return render(request, 'dashboard/harrassment_reports.html', {'harrassobj': harrassment_feedback,
                                                                  'media_url':settings.MEDIA_URL})

def register_member(request):
    return render(request, 'dashboard/register_sacco_member.html')

def conductor_upload(request):
    if request.method == 'POST':
        form = ConductorsForm(request.POST,request.FILES)
        if form.is_valid():
            conductor = form.save(commit=False)
            conductor.save()
            return redirect('detailed_report',pk=conductor.pk)
    else:
        form = ConductorsForm()
    return render(request,'dashboard/register_conductor.html',{'form': form})

def driver_upload(request):
    if request.method == 'POST':
        form = DriversForm(request.POST,request.FILES)
        if form.is_valid():
            driver = form.save(commit=False)
            driver.save()
            return redirect('detailed_report', pk=driver.pk)
    else:
        form = DriversForm()
    return render(request,'dashboard/register_driver.html',{'form': form})

def vehicle_upload(request):
    if request.method == 'POST':
        form = VehicleForm(request.POST,request.FILES)
        if form.is_valid():
            vehicle = form.save(commit=False)
            vehicle.save()
            return redirect('detailed_report')
    else:
        form = VehicleForm()
    return render(request,'dashboard/register_vehicle.html',{'form': form})


def search_sacco(request):
    return render(request, 'dashboard/search_form.html')

def search_route(request):
    sentiment_analysis.message_classification()
    return render(request, 'dashboard/critical_search_form.html')

def search_route_details(request):
    return render(request, 'dashboard/search_route_details.html')

def critical_results(request):
    if request.method == 'GET':
        query = request.GET.get('route',None)

        driverfeedtoday = Drivers.objects.raw(
        '''SELECT * FROM dashboard_drivers 
           INNER JOIN dashboard_feedbackgiven ON dashboard_drivers.number_plate = dashboard_feedbackgiven.number_plate
           INNER JOIN dashboard_vehicles ON dashboard_feedbackgiven.number_plate = dashboard_vehicles.number_plate
           INNER JOIN dashboard_saccos ON dashboard_vehicles.sacco_name = dashboard_saccos.sacco_name
           WHERE dashboard_saccos.route_number = %s''',[query])


        # object_list = Drivers.objects.filter(number_plate=query)
        return render(request,'dashboard/search_results_critical.html',{'driverfeedtoday': driverfeedtoday,
                                                               'media_url':settings.MEDIA_URL})
    else:
        render('dashboard/search_form.html')
        
def search_results(request):    
    if request.method == 'GET':
        query = request.GET.get('sacco',None)
        
       


        #driver rankings
        pos = "Positive"
        neg = "Negative"
        drivers_ranking = Drivers.objects.raw(
            '''SELECT dashboard_drivers.id,dashboard_drivers.driver_name,dashboard_vehicles.sacco_name,
                count(*) AS total,
                sum(case when dashboard_feedbackgiven.sentiment = %s then 1 else 0 end) AS PosCount,
                sum(case when dashboard_feedbackgiven.sentiment = %s then 1 else 0 end) AS NegCount
                FROM public.dashboard_feedbackgiven
                INNER JOIN public.dashboard_drivers
                ON dashboard_feedbackgiven.number_plate = dashboard_drivers.number_plate
                INNER JOIN public.dashboard_vehicles
                ON dashboard_feedbackgiven.number_plate = dashboard_vehicles.number_plate
                WHERE dashboard_vehicles.sacco_name = %s
                GROUP BY dashboard_drivers.id,dashboard_drivers.driver_name,dashboard_vehicles.sacco_name
            ''',[pos,neg,query]
        )

        default_rank = 5
        for d in drivers_ranking:
            pos_rate = d.poscount
            neg_rate = d.negcount
            new_count = default_rank+(pos_rate*0.25)-(neg_rate*0.25)

            d.count = new_count

        disabled_about = '%disabled%'
        blind_about = '%blind%'
        harrassment_feedback = FeedbackGiven.objects.raw(
        '''SELECT dashboard_vehicles.number_plate,dashboard_vehicles.id,dashboard_vehicles.sacco_name,
                dashboard_feedbackgiven.feedback_about,COUNT(*)
            FROM dashboard_vehicles 
            INNER JOIN dashboard_feedbackgiven ON dashboard_feedbackgiven.number_plate 
                = dashboard_vehicles.number_plate
            INNER JOIN dashboard_saccos ON LOWER(dashboard_vehicles.sacco_name) 
                = LOWER(dashboard_saccos.sacco_name)
            WHERE dashboard_vehicles.sacco_name = %s
                AND public.dashboard_feedbackgiven.feedback_body LIKE %s
                OR public.dashboard_feedbackgiven.feedback_body LIKE %s
            GROUP BY dashboard_vehicles.number_plate,dashboard_feedbackgiven.feedback_about,
                dashboard_vehicles.id,dashboard_vehicles.sacco_name
        ''',[query,disabled_about,blind_about])

       
        #against women
        women_about = '%women%'
        harrassment_feed = FeedbackGiven.objects.raw(
        '''SELECT dashboard_vehicles.number_plate,dashboard_vehicles.id,dashboard_vehicles.sacco_name,
                dashboard_feedbackgiven.feedback_about,COUNT(*)
            FROM dashboard_vehicles 
            INNER JOIN dashboard_feedbackgiven ON dashboard_feedbackgiven.number_plate 
                = dashboard_vehicles.number_plate
            INNER JOIN dashboard_saccos ON LOWER(dashboard_vehicles.sacco_name) 
                = LOWER(dashboard_saccos.sacco_name)
            WHERE dashboard_vehicles.sacco_name = %s
                AND public.dashboard_feedbackgiven.feedback_body LIKE %s
            GROUP BY dashboard_vehicles.number_plate,dashboard_feedbackgiven.feedback_about,
                dashboard_vehicles.id,dashboard_vehicles.sacco_name
        ''',[query,women_about])
        return render(request,'dashboard/search_results.html',{'drivers_ranking': drivers_ranking,
                                                               'harrassobj': harrassment_feedback,
                                                               'harrasswmn': harrassment_feed,
                                                               'media_url':settings.MEDIA_URL})
    else:
        render('dashboard/search_form.html')

def detailed_report(request,num):
    about= 'driver'

    driver_profile = Drivers.objects.raw(
        '''SELECT * FROM dashboard_drivers
         INNER JOIN dashboard_vehicles ON dashboard_drivers.number_plate = dashboard_vehicles.number_plate
           WHERE dashboard_drivers.number_plate = %s 
           ''',[num])
    
    driver_feed = Drivers.objects.raw(
        '''SELECT * FROM dashboard_drivers 
           INNER JOIN dashboard_feedbackgiven ON dashboard_drivers.number_plate = dashboard_feedbackgiven.number_plate
           INNER JOIN dashboard_vehicles ON dashboard_feedbackgiven.number_plate = dashboard_vehicles.number_plate
           INNER JOIN dashboard_saccos ON dashboard_vehicles.sacco_name = dashboard_saccos.sacco_name
           WHERE dashboard_drivers.number_plate = %s
           AND cast(dashboard_feedbackgiven.day_received as date) = current_date ''',[num])

    return render(request, 'dashboard/specific_driver_report.html',{'detailed_driver': driver_profile,
                                                                    'driver_feed' : driver_feed,
                                                                    'media_url':settings.MEDIA_URL})
def detailed_weekly_report(request,num):
    about= 'driver'

    driver_profile = Drivers.objects.raw(
        '''SELECT * FROM dashboard_drivers
         INNER JOIN dashboard_vehicles ON dashboard_drivers.number_plate = dashboard_vehicles.number_plate
           WHERE dashboard_drivers.number_plate = %s 
           ''',[num])
    
    driver_feed = Drivers.objects.raw(
        '''SELECT * FROM dashboard_drivers 
           INNER JOIN dashboard_feedbackgiven ON dashboard_drivers.number_plate = dashboard_feedbackgiven.number_plate
           INNER JOIN dashboard_vehicles ON dashboard_feedbackgiven.number_plate = dashboard_vehicles.number_plate
           INNER JOIN dashboard_saccos ON dashboard_vehicles.sacco_name = dashboard_saccos.sacco_name
           WHERE dashboard_drivers.number_plate = %s 
           AND cast(dashboard_feedbackgiven.day_received as date) >= now() - interval '1 week'
           ''',[num])

    return render(request, 'dashboard/specific_driver_report.html',{'detailed_driver': driver_profile,
                                                                    'driver_feed' : driver_feed,
                                                                    'media_url':settings.MEDIA_URL})
def detailed_monthly_report(request,num):
    about= 'driver'

    driver_profile = Drivers.objects.raw(
        '''SELECT * FROM dashboard_drivers
         INNER JOIN dashboard_vehicles ON dashboard_drivers.number_plate = dashboard_vehicles.number_plate
           WHERE dashboard_drivers.number_plate = %s 
           ''',[num])
    
    driver_feed = Drivers.objects.raw(
        '''SELECT * FROM dashboard_drivers 
           INNER JOIN dashboard_feedbackgiven ON dashboard_drivers.number_plate = dashboard_feedbackgiven.number_plate
           INNER JOIN dashboard_vehicles ON dashboard_feedbackgiven.number_plate = dashboard_vehicles.number_plate
           INNER JOIN dashboard_saccos ON dashboard_vehicles.sacco_name = dashboard_saccos.sacco_name
           WHERE dashboard_drivers.number_plate = %s 
           AND cast(dashboard_feedbackgiven.day_received as date) >= date_trunc('month', current_date) 
           ''',[num])

    return render(request, 'dashboard/specific_driver_report.html',{'detailed_driver': driver_profile,
                                                                    'driver_feed' : driver_feed,
                                                                    'media_url':settings.MEDIA_URL})

def detailed_conductor_report(request,num):
    about= 'conductor'

    conductor_profile = Conductors.objects.raw(
        '''SELECT * FROM dashboard_conductors 
           WHERE dashboard_conductors.number_plate = %s 
           ''',[num])

    conductor_feed = Conductors.objects.raw(
        '''SELECT * FROM dashboard_conductors 
           INNER JOIN dashboard_feedbackgiven ON dashboard_conductors.number_plate = dashboard_feedbackgiven.number_plate
           INNER JOIN dashboard_vehicles ON dashboard_feedbackgiven.number_plate = dashboard_vehicles.number_plate
           INNER JOIN dashboard_saccos ON dashboard_vehicles.sacco_name = dashboard_saccos.sacco_name
           WHERE dashboard_conductors.number_plate = %s 
           AND
           dashboard_feedbackgiven.feedback_about =%s ''',[num,about])

    
    return render(request, 'dashboard/specific_conductor_report.html',{'detailed_conductor': conductor_profile,
                                                                        'conductor_feed' : conductor_feed,
                                                                        'media_url':settings.MEDIA_URL})

def detailed_vehicle_report(request,num):
    detailed_vehicle = Vehicles.objects.raw(
        '''SELECT * FROM dashboard_vehicles 
           INNER JOIN dashboard_feedbackgiven ON dashboard_vehicles.number_plate = dashboard_feedbackgiven.number_plate 
           WHERE dashboard_vehicles.number_plate = %s''',[num])

    
    return render(request, 'dashboard/specific_vehicle_report.html',{'detailed_vehicle': detailed_vehicle,'media_url':settings.MEDIA_URL})

def detailed_sacco_report(request,num):
    detailed_sacco_report = Saccos.objects.raw(
        '''SELECT * FROM dashboard_saccos 
            INNER JOIN dashboard_vehicles ON dashboard_saccos.sacco_name = dashboard_vehicles.sacco_name
           INNER JOIN dashboard_feedbackgiven ON dashboard_vehicles.number_plate = dashboard_feedbackgiven.number_plate
           WHERE dashboard_saccos.sacco_name = %s ''',[num])

    return render(request, 'dashboard/specific_sacco_report.html',{'detailed_sacco_report': detailed_sacco_report,
                                                                    'media_url':settings.MEDIA_URL})
def detailed_daily_sacco_report(request,num):
    detailed_sacco_report = Saccos.objects.raw(
        '''SELECT * FROM dashboard_saccos 
            INNER JOIN dashboard_vehicles ON dashboard_saccos.sacco_name = dashboard_vehicles.sacco_name
           INNER JOIN dashboard_feedbackgiven ON dashboard_vehicles.number_plate = dashboard_feedbackgiven.number_plate
           WHERE dashboard_saccos.sacco_name = %s 
           AND cast(dashboard_feedbackgiven.day_received as date) = current_date''',[num])

    return render(request, 'dashboard/specific_sacco_report.html',{'detailed_sacco_report': detailed_sacco_report,
                                                                    'media_url':settings.MEDIA_URL})

def detailed_week_sacco_report(request,num):
    detailed_sacco_report = Saccos.objects.raw(
        '''SELECT * FROM dashboard_saccos 
            INNER JOIN dashboard_vehicles ON dashboard_saccos.sacco_name = dashboard_vehicles.sacco_name
           INNER JOIN dashboard_feedbackgiven ON dashboard_vehicles.number_plate = dashboard_feedbackgiven.number_plate
           WHERE dashboard_saccos.sacco_name = %s 
           AND cast(dashboard_feedbackgiven.day_received as date) >= now() - interval '1 week'
           ''',[num])

    return render(request, 'dashboard/specific_sacco_report.html',{'detailed_sacco_report': detailed_sacco_report,'media_url':settings.MEDIA_URL})

def detailed_monthly_sacco_report(request,num):
    detailed_sacco_report = Saccos.objects.raw(
        '''SELECT * FROM dashboard_saccos 
            INNER JOIN dashboard_vehicles ON dashboard_saccos.sacco_name = dashboard_vehicles.sacco_name
           INNER JOIN dashboard_feedbackgiven ON dashboard_vehicles.number_plate = dashboard_feedbackgiven.number_plate
           WHERE dashboard_saccos.sacco_name = %s 
           AND cast(dashboard_feedbackgiven.day_received as date) >= now() - interval '1 week'
           ''',[num])

    return render(request, 'dashboard/specific_sacco_report.html',{'detailed_sacco_report': detailed_sacco_report,'media_url':settings.MEDIA_URL})


def detailed_harrassment_report(request,num):
    disabled_about = '%disabled%'
    blind_about = '%blind%'
    harrassment_feedback = FeedbackGiven.objects.raw(
    '''SELECT dashboard_vehicles.number_plate,dashboard_vehicles.id,dashboard_vehicles.sacco_name,
            dashboard_feedbackgiven.feedback_about,
        FROM dashboard_vehicles 
        INNER JOIN dashboard_feedbackgiven ON dashboard_feedbackgiven.number_plate 
            = dashboard_vehicles.number_plate
        INNER JOIN dashboard_saccos ON LOWER(dashboard_vehicles.sacco_name) 
            = LOWER(dashboard_saccos.sacco_name)
        WHERE dashboard_vehicles.sacco_name = %s
            AND public.dashboard_feedbackgiven.feedback_body LIKE %s
            OR public.dashboard_feedbackgiven.feedback_body LIKE %s
    ''',[disabled_about,blind_about])

    return render(request, 'dashboard/specific_harrassment_report.html',{'harrassment_feedback': harrassment_feedback,'media_url':settings.MEDIA_URL})


def display_charts(request):
    return render(request, 'dashboard/chart-c3.html')

def list_feedback(request):
    return render(request, 'dashboard/list_vehicles.html')

class ListFeedbackGiven(viewsets.ModelViewSet):
    queryset = FeedbackGiven.objects.all()
    serializer_class = FeedbackGivenSerializer

class ListSaccos(viewsets.ModelViewSet):
    queryset = Saccos.objects.all()
    serializer_class = SaccosSerializer

class ListMatatuRoutes(viewsets.ModelViewSet):
    queryset = MatatuRoutes.objects.all()
    serializer_class = MatatuRoutesSerializer

class ListVehicles(viewsets.ModelViewSet):
    queryset = Vehicles.objects.all()
    serializer_class = VehiclesSerializer

class ListDrivers(viewsets.ModelViewSet):
    queryset = Drivers.objects.all()
    serializer_class = DriversSerializer
class ListConductors(viewsets.ModelViewSet):
    queryset = Conductors.objects.all()
    serializer_class = ConductorsSerializer

#respond to text
@csrf_exempt
def sms_response(request):
    # Start our TwiML response
    resp = MessagingResponse()

    # Add a text message
    msg = resp.message("Thank you for your feedback to Tripwatch!")    
    
    return HttpResponse(str(resp))