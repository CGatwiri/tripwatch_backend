# Generated by Django 3.0.2 on 2020-03-11 21:46

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='FeedbackGiven',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('day_received', models.CharField(max_length=200)),
                ('time_sent', models.CharField(max_length=200)),
                ('number_plate', models.CharField(max_length=200)),
                ('feedback_about', models.CharField(max_length=200)),
                ('feedback_body', models.CharField(max_length=200)),
                ('sentiment', models.CharField(max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='MatatuRoutes',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('route_number', models.CharField(max_length=200)),
                ('county', models.CharField(max_length=200)),
                ('constituency', models.CharField(max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='Saccos',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('sacco_name', models.CharField(max_length=200)),
                ('route_number', models.ForeignKey(db_column='route_number', on_delete=django.db.models.deletion.CASCADE, to='dashboard.MatatuRoutes')),
            ],
        ),
        migrations.CreateModel(
            name='Vehicles',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('number_plate', models.CharField(max_length=200)),
                ('sacco_name', models.ForeignKey(db_column='sacco_name', on_delete=django.db.models.deletion.CASCADE, to='dashboard.Saccos')),
            ],
        ),
        migrations.CreateModel(
            name='StageManager',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('stageman_name', models.CharField(max_length=200)),
                ('year_joined', models.CharField(max_length=200)),
                ('stage_manager_picture', models.ImageField(default=None, upload_to='images/stagemanager')),
                ('sacco_name', models.ForeignKey(db_column='sacco_name', on_delete=django.db.models.deletion.CASCADE, to='dashboard.Saccos')),
            ],
        ),
        migrations.CreateModel(
            name='SaccoMembers',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('username', models.CharField(max_length=200)),
                ('phone_no', models.CharField(max_length=200)),
                ('email_address', models.CharField(max_length=200)),
                ('sacco_name', models.ForeignKey(db_column='sacco_name', on_delete=django.db.models.deletion.CASCADE, to='dashboard.Saccos')),
            ],
        ),
        migrations.CreateModel(
            name='PoliceOfficers',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('police_name', models.CharField(max_length=200)),
                ('year_joined', models.CharField(max_length=200)),
                ('route_number', models.ForeignKey(db_column='route_number', on_delete=django.db.models.deletion.CASCADE, to='dashboard.MatatuRoutes')),
            ],
        ),
        migrations.CreateModel(
            name='Drivers',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('driver_name', models.CharField(max_length=200)),
                ('year_joined', models.CharField(max_length=200)),
                ('driver_picture', models.ImageField(default=None, upload_to='images/drivers')),
                ('number_plate', models.ForeignKey(db_column='number_plate', on_delete=django.db.models.deletion.CASCADE, to='dashboard.Vehicles')),
            ],
        ),
        migrations.CreateModel(
            name='Conductors',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('conductor_name', models.CharField(max_length=200)),
                ('year_joined', models.CharField(max_length=200)),
                ('conductor_picture', models.ImageField(default=None, upload_to='images/conductors')),
                ('number_plate', models.ForeignKey(db_column='number_plate', on_delete=django.db.models.deletion.CASCADE, to='dashboard.Vehicles')),
                ('sacco_name', models.ForeignKey(db_column='sacco_name', on_delete=django.db.models.deletion.CASCADE, to='dashboard.Saccos')),
            ],
        ),
    ]
