from django.contrib import admin

from .models import FeedbackGiven, Saccos, MatatuRoutes, Vehicles, Drivers

# Register your models here.
admin.site.register(FeedbackGiven)
admin.site.register(Saccos)
admin.site.register(MatatuRoutes)
admin.site.register(Vehicles)
admin.site.register(Drivers)