try:
    import twilio_connection
except:
    import dashboard.twilio_connection as twilio_connection
    
import requests 
import uuid 
import re
from .models import FeedbackGiven
import time
from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer 

feedback_id = uuid.uuid4() 

url = "http://localhost:8000/dash/api/feedback/"


# function to print sentiments 
# of the sentence. 
def sentiment_scores(sentence): 

    # Create a SentimentIntensityAnalyzer object. 
    sid_obj = SentimentIntensityAnalyzer() 

    # polarity_scores method of SentimentIntensityAnalyzer 
    # oject gives a sentiment dictionary. 
    # which contains pos, neg, neu, and compound scores. 
    sentiment_dict = sid_obj.polarity_scores(sentence) 

    print("Overall sentiment dictionary is : ", sentiment_dict) 
    print("sentence was rated as ", sentiment_dict['neg']*100, "% Negative") 
    print("sentence was rated as ", sentiment_dict['neu']*100, "% Neutral") 
    print("sentence was rated as ", sentiment_dict['pos']*100, "% Positive") 

    print("Sentence Overall Rated As", end = " ") 

    # decide sentiment as positive, negative and neutral 
    if sentiment_dict['compound'] >= 0.05 :  
        sentiment = "Positive"
        print(sentiment) 
        
    elif sentiment_dict['compound'] <= - 0.05 :
        sentiment = "Negative"
        print(sentiment)  

    else : 
        sentiment = "Neutral"
        print(sentiment)  
    
    return sentiment

def fetch_and_analyse_new_messages():
    for myrecord in twilio_connection.messages:
        daysent = myrecord.date_sent

        # Get the date object from datetime object
        dateObj = daysent.date()
        timeObj = daysent.time()
        feedback = myrecord.body

        #do analysis on feedback body to see if it meets format
        #1.Check if number plate is present
        expected_format = '[Kk][a-zA-Z]{2}[0-9]{3}[a-zA-Z]{1}[a-zA-Z]*'
        pattern = re.compile(expected_format)

        if(pattern.match(feedback)):
            #2.Check if word driver, conductor or stageman is present
            driver_string= "driver"
            conductor_string= "conductor"
            stageman_string = "stageman"
            vehicle_s = "vehicle"
            vehicle_string = ["vehicle","matatu","car"]
            # women_string = "ladies" or "women" or "woman" or "girl"
            women_string = ["women", "ladies", "woman", "girl"]
            # pwd_string = "blind" or "wheelchair" or "deaf" or "mute" or "autistic"
            pwd_string = ["disabled","blind", "wheelchair","deaf","mute","autistic"]
            
            if driver_string in feedback:
                feedback_about = driver_string

            elif conductor_string in feedback:
                feedback_about = conductor_string

            elif stageman_string in feedback:
                feedback_about = stageman_string
            
            elif vehicle_s in feedback:
                feedback_about = vehicle_s
            else:
                feedback_about = "Vehicle"

            # elif any(v in vehicle_string for v in feedback):
            #     feedback_about = vehicle_s

            # if any(w in women_string for w in feedback):
            #     feedback_about = women_string

            # if any(p in pwd_string for p in feedback):
            #     feedback_about = women_string

            #3. Get number plate from text
            number_plate = feedback.split()
            number_plate = [ i.upper() for i in number_plate]
            #4. Sentiment of the feedback  
            # Driver code 
            # if __name__ == "__main__" : 

            print("\nFeedback statement :")
            print(feedback) 
            sentence = feedback 

            # function calling 
            sentiment = sentiment_scores(sentence) 

        #3. Post to feedback url
            myobj = {'day_received': dateObj,
                    'time_sent': timeObj,
                    'number_plate': number_plate[0],
                    'feedback_about': feedback_about,
                    'feedback_body': feedback,
                    'sentiment': sentiment}

            if FeedbackGiven.objects.filter(day_received=dateObj, time_sent=timeObj).exists():
                print("not posted")
            else:
                x = requests.post(url, data = myobj)
                print('success!')