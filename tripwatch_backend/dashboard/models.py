from django.db import models

# Create your models here.

class MatatuRoutes(models.Model):
    route_number = models.CharField(max_length=200)
    county = models.CharField(max_length=200)
    constituency = models.CharField(max_length=200)

    def __str__(self):
        return self.route_number

class Saccos(models.Model):
    sacco_name = models.CharField(max_length=200)
    route_number = models.CharField(max_length=200)

    def __str__(self):
        return self.sacco_name

class Vehicles(models.Model):
    number_plate = models.CharField(max_length=200)
    sacco_name = models.CharField(max_length=200)

    def __str__(self):
        return self.number_plate

class FeedbackGiven(models.Model):
    day_received = models.CharField(max_length=200)
    time_sent = models.CharField(max_length=200)
    number_plate = models.CharField(max_length=200)
    feedback_body = models.CharField(max_length=200)
    sentiment = models.CharField(max_length=200)
    
class Drivers(models.Model):
    driver_name = models.CharField(max_length=200)
    number_plate = models.CharField(max_length=200)
    year_joined = models.CharField(max_length=200)
    driver_picture = models.ImageField(upload_to = 'images/drivers', default=None)


class Conductors(models.Model):
    conductor_name = models.CharField(max_length=200)
    number_plate = models.CharField(max_length=200)
    sacco_name = models.CharField(max_length=200)
    # sacco_name = models.ForeignKey(Saccos,db_column='sacco_name', on_delete=models.CASCADE)
    year_joined = models.CharField(max_length=200)
    conductor_picture = models.ImageField(upload_to = 'images/conductors', default=None)



