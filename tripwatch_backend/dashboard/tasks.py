from background_task import background
import dashboard.feedback_processing as feedback

@background(schedule=6)
def postdata():
   feedback.fetch_and_analyse_new_messages()

postdata()