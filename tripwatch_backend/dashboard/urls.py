from django.urls import path, include
from rest_framework import routers
from . import views

router = routers.DefaultRouter()
router.register(r'feedback', views.ListFeedbackGiven)
router.register(r'saccos', views.ListSaccos)
router.register(r'vehicles', views.ListVehicles)
router.register(r'drivers', views.ListDrivers)
router.register(r'matatu_routes', views.ListMatatuRoutes)
router.register(r'conductors', views.ListConductors)

urlpatterns = [
    path('api/', include(router.urls)),
    path('homeeee/', views.search_route, name="homeeee" ),
    path('home/',views.home, name="home"),
    path('adminhome/',views.adminhome, name="adminhome"),
    path('homeee/', views.index, name="homeee" ),
    path('admin_feedback_today/', views.admin_feedback_today, name="admin_feedback_today" ),
    path('weekly_report/', views.weekly_report, name="weekly_report" ),
    path('monthly_report/', views.monthly_report, name="monthly_report" ),
    path('vehicle/', views.vehicle, name="vehicle"),
    path('drivers/', views.drivers, name="drivers"),
    path('detailed_report/<slug:num>', views.detailed_report, name="detailed_report"),
    path('detailed_weekly_report/<slug:num>', views.detailed_weekly_report, name="detailed_weekly_report"),
    path('detailed_monthly_report/<slug:num>', views.detailed_monthly_report, name="detailed_monthly_report"),
    path('detailed_conductor_report/<slug:num>', views.detailed_conductor_report, name="detailed_conductor_report"),
    path('detailed_vehicle_report/<slug:num>', views.detailed_vehicle_report, name="detailed_vehicle_report"),
    path('detailed_sacco_report/<slug:num>', views.detailed_sacco_report, name="detailed_sacco_report"),
    path('detailed_daily_sacco_report/<slug:num>', views.detailed_daily_sacco_report, name="detailed_daily_sacco_report"),
    path('detailed_week_sacco_report/<slug:num>', views.detailed_week_sacco_report, name="detailed_week_sacco_report"),
    path('detailed_monthly_sacco_report/<slug:num>', views.detailed_monthly_sacco_report, name="detailed_monthly_sacco_report"),
    path('detailed_harrassment_report/<slug:num>', views.detailed_harrassment_report, name="detailed_harrassment_report"),
    path('conductor/', views.conductor, name="conductor"),
    path('stageman/', views.stageman, name="stageman"),
    path('saccos/', views.sacco, name="sacco"),
    path('harrassment/', views.harrassment, name="harrassment"),
    path('register_member/', views.register_member, name="register_member"),
    path('register_driver/', views.driver_upload, name="register_driver"),
    path('register_conductor/', views.conductor_upload, name="register_conductor"),
    path('register_vehicle/', views.vehicle_upload, name="register_vehicle"),
    path('display_charts/', views.display_charts, name="display_charts"),
    path('search_sacco/', views.search_sacco, name="search_sacco"),
    path('search_results/', views.search_results, name="search_results"),
    path('search_route/', views.search_route, name="search_route"),
    path('search_route_details/', views.search_route_details, name="search_route_details"),
    path('critical_results/', views.critical_results, name="critical_results"),
    path('repeat_sacco/', views.repeat_sacco, name="repeat_sacco"),

    path('sms', views.sms_response, name="sms")
    
]