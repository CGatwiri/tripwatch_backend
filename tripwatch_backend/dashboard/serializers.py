from rest_framework import serializers

from .models import FeedbackGiven, Saccos,  MatatuRoutes, Vehicles, Drivers, Conductors

class FeedbackGivenSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = FeedbackGiven
        fields = '__all__'

class SaccosSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Saccos
        fields = '__all__'

class ConductorsSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Conductors
        fields = '__all__'

class MatatuRoutesSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = MatatuRoutes
        fields = '__all__'

class VehiclesSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Vehicles
        fields = '__all__'

class DriversSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Drivers
        fields = '__all__'